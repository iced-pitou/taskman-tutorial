import * as express from 'express';
import * as postgres from 'pg';
import * as dbClient from './src/config/db-client.json';
import * as dbQuery from './src/config/db-query.json';
import Task from './src/model/task';

// Configuration
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static('src/public'));

const client = new postgres.Client(dbClient);


// TODO use routes and controllers instead
// GET all
app.get('/api/task', async (req, res) => {
    try {
        const result = await client.query(dbQuery.task.selectAll);
        res.status(200).send(result.rows);
    } catch (error) {
        console.log(error);
    };
});

// POST
app.post('/api/task', async (req, res) => {
    const text: string = (req.body as Task).text.trim();
    try {
        if (!text) throw new Error("Empty text");
        await client.query(dbQuery.task.insertNew, [text]);
        res.status(200);
    } catch (error) {
        console.log((error as Error).message);
        res.status(500);
    };
    res.redirect('/');
});

// Starting
const port = 3000;
app.listen(port, () => {
    console.log(`App listening on port ${port}...`);
    client.connect();
});

// Shutdown event-handling
for (let signal of ['SIGINT', 'SIGTERM']) {
    process.on(signal, () => {
        console.log('Application shutdown');
        client.end();
        process.exit();
    });
}
