/* Task */

/* Table */
CREATE TABLE task (
    text VARCHAR(25) PRIMARY KEY,
    done BOOLEAN DEFAULT false
);

/* Insert */
INSERT INTO task (text) VALUES ('Create a new task');