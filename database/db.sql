/* Execute with role "psql -U postgres -f database/db.sql" */

/* Conditional drops */
DROP DATABASE IF EXISTS taskman;
DROP ROLE IF EXISTS taskman;

/* User */
CREATE ROLE taskman WITH LOGIN  ENCRYPTED PASSWORD 'taskman' CREATEDB SUPERUSER;

/* Database */
CREATE DATABASE taskman OWNER taskman;

/* Privileges */
GRANT ALL PRIVILEGES ON DATABASE taskman to taskman;

/* Connect */
\c postgresql://taskman:taskman@localhost:5432;

/* Tables */
\ir task.sql