export default class Response {

    message: string;
    value: object;

    constructor(message: string, value: object) {
        this.message = message;
        this.value = value;
    }

}